---
title: "Emoji Support"
date: 2020-06-19T21:12:04+02:00
draft: false
---

Es gibt fucking Emoji-Support!
Der erste Link gibt ne kurze Beschreibung von der Github-Seite des Themes, der zweite Link fuehrt zu einem CheatSheet mit den ganzen Namen der Emojis.

- [Gittihubbi](https://github.com/cntrump/hugo-notepadium/blob/master/exampleSite/content/post/emoji-support.md)
- [Emoji CheatSheet](https://www.webfx.com/tools/emoji-cheat-sheet/) :heart:
