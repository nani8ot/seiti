---
title: "Serverprobleme"
date: 2020-06-19T21:00:19+02:00
categories: [
    "Recommended",
    ]
tags: [
    "server",
    ]
draft: false
---

Falls es irgendwelche Probleme mit einem Dienst geben sollte, hat es wahrscheinlich Aaron (Ich) mal wieder verbockt. So von wegen rm -R / und so xD
Da hilft dann ganz laut schreien und vielleicht eine Nachricht per Discord schreiben. Oder per Mail. Oder per Messenger.

Die Internetanbindung ist dank DSL jetzt alles andere als Gigabit, deshalb bitte nicht mitten am Tag zehn Gigabyte an Zeug hoch oder runterladen. Das faellt dann unter "nichts kaputt machen" und dauert sowieso ewig. Die Gameserver lassen sich durch eine hoehere Latenz nicht mehr gut nutzen.

