#!/bin/bash
if [[ $(id -u) -ne 0 ]] ; then echo "Please run as root" ; exit 1 ; fi
echo "> Fetching Data"
sudo -u www-data git fetch
sudo -u www-data git reset --hard origin/master
echo "> Purging Data"
sudo -u www-data rm -Rf public
echo "> Generating Website"
sudo -u www-data hugo
sudo -u www-data rm -Rf ../content
sudo -u www-data mv public ../content
echo "> Restarting Services"
cd ../ && docker-compose down && docker-compose up -d
echo "> Finished"
